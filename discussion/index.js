// index.js


console.log("Hello once more");

// Initialization of Variables
let hello;
console.log(hello); //result: undefined

// console.log(x); //result: err
// let x;

// Declaration of Variables
let firstName = "Jin"; //good variable name
let pokemon = 25000; //bad variable name

let FirstName = "Izuku"; //bad variable name
let firstNameAgain = "All Might"; //good variable name

// let first name = "Midoriya"; //bad variable name

// camelcase
	//lastName, emailAddress, mobileNumber
// underscores
let product_description = "lorem ipsum";
let product_id = "250000ea1000"

// Syntax:
// 	let / const variableName = value;

let productName = "desktop computer";
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

const pi = 3.14;
console.log(pi);

// Reassigning variable
// Syntax:
// 	variableName = value;

productName = "Laptop";
console.log(productName); //result: Laptop

let friend = "Kate";
friend = "Chance";
console.log(friend);

// let friend = "Jane";
console.log(friend);

// pi = 3.1;
// console.log(pi); //result: err "assignment to constant variable"

// Reassigning versus Initializing variable

// initialize
let supplier;
supplier = "Jane Smith Tradings";
console.log(supplier);

// reassignment
supplier = "Zuitt Store";
console.log(supplier);

// var vs. let/const

a = 5;
console.log(a);
var a;

b = 6;
console.log(b);
// let b;

// local and global scope

let outerVariable = "hello";

{
	let innervariable = "hello again";
	var anothervariable = "let me out";
}

console.log(outerVariable);
// console.log(innervariable);
// console.log(anothervariable);

// Multiple Variable Declaration

let productCode = "DC017", productBrand = "Dell";
console.log(productCode,productBrand);


// let is a reserved keyword
// const let = "hello";
// console.log(let);

// Data Types

// string

let country = "Philippines";
let city = "Manila City";

console.log (city, ",", country);

// Concatenating Strings

let fullAdress = city + ", " + country
console.log(fullAdress);

console.log(city, ",", country)

let myName = "elaine";
let greeting = "Hi I am " + myName;
console.log(greeting);


// escape character (\)	// "\n"

let mailAddress = "Metro Manila\n\nPhilippines";
console.log(mailAddress);


let message = "how's it going \\bruh";
console.log(message);

// numbers integers whole numbers decimal exp

let headcount = 26;
console.log(headcount);
let grade = 74.9;
console.log(grade);
let planet_distance = 2e10;
console.log(planet_distance);

// boolean

let isMarried = false;
let isSingle = true;
console.log("isMarried " + isMarried);
console.log("isSingle " + isSingle);

// arrays

let grades = [98.7, 77.3, 90.8, 88.4];
const anime = ["BNHA", "AoT", "SxF", "KNY"];
console.log(grades);
console.log(anime);

let random = ["JK", 24, true];
console.log(random);

// objects data type

/*
	Syntax:
		let/const objectName = {
			propertyA : valueA,
			propertyB : valueB
		}
*/

let person = {
	fullName: "Edward Scissorhands",
	age: 35,
	isMarried: false,
	contact: ["09123456789", "8123 7567"],
	address: {
		houseNumber: "345",
		city: "Manila"
	}
};
console.log(person);

const myGrades = {
	firstGrading: 98.7,
	secondGrading: 77.3,
	thirdGrading: 88.6,
	fourthGrading: 74.9
};
console.log(myGrades);
// console.log(firstGrading); //error (is not defined)



// typeof operator
console.log(typeof myGrades); //result: Object
console.log(typeof grades);
console.log(typeof grade);

// anime = ["One Punch Man"];
// console.log(anime);

anime[0] = "One Punch Man";
console.log(anime);

// null
let spouse = null;
console.log(spouse); //result: null