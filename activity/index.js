// index.js


let first_name = "Joan";
let last_name = "Jett";
let age = 63;
let hobbies = ["Sports", "Reading", "Going to see Bands"];
let work_address = {
	houseNumber: 636,
	street: "Broadway",
	city: "New York City",
	state: "New York"
};

console.log("First Name: " + first_name);
console.log("Last Name: " + last_name);
console.log("Age: " + age);
console.log("Hobbies:");
console.log(hobbies);
console.log("Work Address:");
console.log(work_address);


let fullName = "Steve Rogers";
console.log("My full name is" + fullName);

let currentAge = 40;
console.log("My current age is: " + currentAge);

let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
console.log("My Friends are: ")
console.log(friends);

let profile = {
	username: "captain_america",
	fullName: "Steve Rogers",
	age: 40,
	isActive: false

};
console.log("My Full Profile: ")
console.log(profile);

let bestfriend = "Bucky Barnes";
console.log("My bestfriend is: " + bestfriend);

let lastLocation = "Arctic Ocean";
lastLocation = "Atlantic Ocean";
console.log("I was found frozen in: " + lastLocation);